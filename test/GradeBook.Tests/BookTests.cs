using Xunit;
using GradeBook;
namespace GradeBook.Tests;
using System;

public class BookTests
{
    [Fact]
    public void Test1()
    {
        //arrange assumtions and expected
        var book = new Book("new book");
        book.AddGrade(45);
        book.AddGrade(30);
        book.AddGrade(35);
        double AverageExpected = 45+30+35;
        AverageExpected /= 3;
        var MinExpected=30;
        var MaxExpected=45;
        //actual what your code do
        var actual = book.getStats();
        //assert
        Assert.Equal(AverageExpected,actual.Average);
        Assert.Equal(MinExpected,actual.Min);
        Assert.Equal(MaxExpected,actual.Max);
    }
}