﻿// See https://aka.ms/new-console-template for more information
using GradeBook;
void OnGradeAdded(object sender, EventArgs e)
{
    Console.WriteLine("Grade is Added to book object.");
}
void calcualateGradeAverage(){
    var book  = new Book("new book");
    book.GradeAdded += OnGradeAdded;
    while (true)
    {
        Console.WriteLine("Enter a grade or 'q' to quit.");
        var input = Console.ReadLine();
        if(input == "q")
        {
            break;
        }
        try
        {
            if(!String.IsNullOrEmpty(input))
            {
                var grade = double.Parse(input);
        book.AddGrade(grade);
            }
              
        }catch(Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
         //   Console.WriteLine("try again!");
        }
      
    }
  
    Console.WriteLine("Min is "+ book.getStats().Min);
    Console.WriteLine("Max is "+ book.getStats().Max);
    Console.WriteLine("Average is "+ book.getStats().Average.ToString("#.##"));
}
calcualateGradeAverage();

