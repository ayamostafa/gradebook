using System;
using System.Collections.Generic;

namespace GradeBook
{
    public delegate void GradeAddedToBookDelegate(object sender, EventArgs args);
    public class Book
    {

        public Book(string name)
        {
            _grades = new List<double>();
            if (string.IsNullOrEmpty(name))
            {
                Name = "";
            }
            else
            {
                Name = name;
            }

        }
        public void AddGrade(double grade)
        {
            if (grade > 0 && grade < 100)
            {
                _grades.Add(grade);
                if (GradeAdded != null)
                {
                    GradeAdded(this, new EventArgs());
                }
            }
            else
            {
                throw new ArgumentException($"Invalid {nameof(grade)}");
            }

        }
        public event GradeAddedToBookDelegate? GradeAdded;
        public Statistics getStats()
        {
            var res = new Statistics();
            var tempMin = double.MaxValue;
            var tempMax = double.MinValue;

            foreach (var grade in _grades)
            {
                if (grade < tempMin)
                {
                    tempMin = grade;
                }
                if (grade > tempMax)
                {
                    tempMax = grade;
                }
                res.Average += grade;
            }
            res.Average /= _grades.Count;
            res.Min = tempMin;
            res.Max = tempMax;
            switch (res.Max)
            {
                case var x when x > 1:
                    Console.WriteLine("Found The Max!");
                    break;
            }



            return res;
        }
        List<double> _grades;
        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_name))
                {
                    return "";
                }
                else
                {
                    return _name;
                }
            }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _name = value;
                }

            }
        }
        private string? _name;
    }
}
